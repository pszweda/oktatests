<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<ul>
    <li>Loader 1: <span class="status">loading...</span></li>
    <li>Loader 2: <span class="status">loading...</span></li>
    <li>Loader 3: <span class="status">loading...</span></li>
    <li>Loader 4: <span class="status">loading...</span></li>
    <li>Loader 5: <span class="status">loading...</span></li>
    <li>Loader 6: <span class="status">loading...</span></li>
    <li>Loader 7: <span class="status">loading...</span></li>
    <li>Loader 8: <span class="status">loading...</span></li>
    <li>Loader 9: <span class="status">loading...</span></li>
    <li>Loader 10: <span class="status">loading...</span></li>
</ul>


<script src="./model.js?date=<?= time() ?>" type="text/javascript"></script>
</body>
</html>